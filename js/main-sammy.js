var app = Sammy('#main', function() {

    this.get('#/', loadInfo);    
    this.get('#/agenda',loadAgenda);
    this.get('#/contacto',loadContacto);
    this.get('#/experiencias',loadExperiencias);
    this.get('#/formularios',loadFormularios);
    this.get('#/convenios',loadConvenios);
    this.get('#/orii',loadInfoOrii);

    function loadInfo(){
        $( "#main" ).load("templates/inicio.html");
    }
    function loadAgenda(){
        $( "#main" ).load("templates/agenda.html");
    }
    function loadContacto(){
        $( "#main" ).load("templates/contacto.html");
    }
    function loadExperiencias(){
        $( "#main" ).load("templates/experiencias.html");
    }
    function loadFormularios(){
        $( "#main" ).load("templates/formularios.html");
    }
    function loadConvenios(){
        $( "#main" ).load("templates/convenios.html");
    }
    function loadInfoOrii(){
        $( "#main" ).load("templates/orii.html");
    }
});

app.run('#/');