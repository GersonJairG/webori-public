function loadContact() {
    ContactModule.getContact().promise()
        .done(function (response) {
            console.log(response);
            contacto = response[0];
            $('#localizacion-ori').html(contacto.ubicacion_ori);
            $('#telefono-contacto').html(contacto.telefono);
            $('#correo-contacto').html(contacto.email);
            $('#fb-contacto').attr('href',contacto.facebook);
            $('#fb-twitter').attr('href',contacto.twitter);
            $('#fb-youtube').attr('href',contacto.youtube);
            $('#fb-instagram').attr('href',contacto.instagram);
        }
        ).fail(function (error) {
            console.log(error);
        })
}
loadContact();

//TODO: terminar