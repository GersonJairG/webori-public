
var AgreementModule = {
    getOptions: function (data) {
        var objects = [];    
        data.forEach(function(agreement) {
            agreement.options = '<button class="edit-option"  data-toggle="modal" data-target="#modal-agreements" agreement-id="'+agreement.id+'" agreement-año="'+agreement.año+'" agreement-tipo="'+agreement.id_tipo_convenio+'"agreement-institucion="'+agreement.id_institucion+'" data-placement="top" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></button><button class="delete-option" agreement-id="'+agreement.id+'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            console.log('CONTROLLER: ')
            console.log(agreement);
            objects.push(agreement);
        });                
        return objects;
    },
    getAgreements: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/convenios',
            data: {

            }
        });
     },
     getCountCountries: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/convenios-mapa/count',
            data: {

            }
        });
     },
     getInfoForCountry: function (country) {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/convenios-mapa/' + country,
            data: {
                country : country
            }
        });
     },    
     getInstitution: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/instituciones',
            data: {

            }
        });
    },    
    createAgreement: function (año, idTipoConvenio, idInstitucion) {
        return $.ajax({
            method: 'POST',
            url: constants.URL_API + '/convenios/new',
            data: {                
                año: año,            
                idTipoConvenio : idTipoConvenio,               
                idInstitucion : idInstitucion,
            }
        })
    },
    deleteAgreement: function (id) {
        return $.ajax({
            method: 'delete',
            url: constants.URL_API + '/convenios/' + id,
            data: {
                id: id
            }
        })
    },
    updateAgreement: function(id, año, idTipoConvenio, idInstitucion){        
        return $.ajax({
            method : 'patch',
            url : constants.URL_API + '/convenios/'+id,
            data : {
                id: id,
                año: año,
                idTipoConvenio : idTipoConvenio,
                idInstitucion : idInstitucion   
            }
        })
    }

}