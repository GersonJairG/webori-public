google.charts.load('current', {
  'packages': ['geochart'],
  // Note: you will need to get a mapsApiKey for your project.
  // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
  'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
});
google.charts.setOnLoadCallback(drawRegionsMap);

// function loadAgreements() {
//   AgreementModule.getAgreements().promise()
//       .done(function (response) {
//           console.log(response);

//       })
//       .fail(function (error) {
//           console.log(error);
//       });
// }


function drawRegionsMap() {

  AgreementModule.getCountCountries().promise()
    .done(function (response) {

      var listContries = [];
      listContries.push(['Paises', 'Convenios']);
      for (var i = 0; i < response.length; i++) {
        var country = [response[i].nombre, response[i].convenios]
        listContries.push(country);
      }
      // console.log(listContries);       
      var data = google.visualization.arrayToDataTable(listContries);

      var options = {
        'title': 'Mapa de convenios',
        'width': 1100,
        'height': 700
      }

      var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

      function selectHandler() {
        var selectedItem = chart.getSelection()[0];
        if (selectedItem) {
          var topping = data.getValue(selectedItem.row, 0);
          $('#my-modal').modal({
            show: 'false'
          });
          $('#title-pais').html(topping);
          AgreementModule.getInfoForCountry(topping).promise()
            .done(function (response) {
              console.log('Trae:');
              console.log(response);
              $('#agreement-unique-table').DataTable({
                "language": {
                  "emptyTable": "No hay datos disponibles en la tabla.",
                  "info": "Registros del _START_ al _END_ de _TOTAL_ ",
                  "infoEmpty": "Mostrando 0 registros de un total de 0.",
                  "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                  "infoPostFix": " ",
                  "lengthMenu": "Mostrar _MENU_ registros",
                  "loadingRecords": "Cargando...",
                  "processing": "Procesando...",
                  "search": "Buscar:",
                  // "searchPlaceholder": "Dato para buscar",
                  "zeroRecords": "No se han encontrado coincidencias.",
                  "paginate": {
                    "first": "Primera",
                    "last": "Última",
                    "next": "Siguiente",
                    "previous": "Anterior"
                  },
                  "aria": {
                    "sortAscending": "Ordenación ascendente",
                    "sortDescending": "Ordenación descendente"
                  }
                },
                "columnDefs": [
                  { "className": "dt-center", "targets": "_all" }
                ],
                "paging": true,
                "filter": true,
                "autoWidth": false,
                'retrieve': true,//permite volver a cargar la tabla con mas valores o los mismos
                'data': response,
                'columns': [
                  { title: "Año", data: 'año' },
                  { title: "Institucion", data: 'institucion' },
                  { title: "Tipo de convenio", data: 'tipo_convenio' }
                ],
                'bLengthChange': false
              });

            })
            .fail(function (error) {
              console.log('ERRORRRRRR');
              console.log(error);
            });
        }
      }

      google.visualization.events.addListener(chart, 'select', selectHandler);
      chart.draw(data, options);
      //Cuando se cierra el modal se limpia la tabla
      $("#my-modal").on('hidden.bs.modal', function () {
        $('#agreement-unique-table').dataTable().fnDestroy();
      });

      //Centrar div contenedor de mapa que es creador desde js
      $(".div_in_regions_div").children("div").children("div").css('margin-left', 'auto');
      $(".div_in_regions_div").children("div").children("div").css('margin-right', 'auto');

    })
    .fail(function (error) {
      console.log(error);
    });



}
