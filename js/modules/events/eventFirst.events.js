//JQUERY 1.11.3 Por eso uso el promise asi y no con el .then y el .catch

function loadEventsFirst() {
    EventModule.getEvents().promise()
        .done(function (response) {
            console.log(response);
            var evt = '';
            var colores = ['blue', 'purple', 'default', 'red'];   
            console.log(colores[0] + "PRUEBAAAAAA" ) ;
            if (response[3] != null) {
                console.log('Ponga los primeros 4');
                var i = 0;
                while (i <= 3) {
                    evt += '<div class="owl-item" style="width: 285px;">';
                    evt += '<div class="item">';
                    evt += '<a href="" style="text-align: center;">';                                      
                    evt += '<div class="easy-block-v1-badge rgba-'+colores[i]+'" style="color:#fff; padding: 5px;">';
                    evt += formatDate(response[i].fecha);
                    evt += '</div>';
                    evt += '<em class="overflow-hidden">';
                    evt += '<img class="img-responsive" src="rsc/img/construccion.jpg" alt="Imagen de eventos">';
                    evt += '</em>';
                    evt += '<span>';
                    evt += '<strong>' + response[i].nombre + '</strong>';
                    evt += '<i>' + response[i].lugar + '</i>';
                    evt += '</span>';
                    evt += '</a>';
                    evt += '</div>';
                    evt += '</div>';    
                    i++;                    
                }
                $('#contenedor-eventos').html(evt);
            } else {
                var i = 0;
                console.log('Recorra los que haya e imprima');
                response.forEach(evento => {
                    evt += '<div class="owl-item" style="width: 285px;">';
                    evt += '<div class="item">';
                    evt += '<a href="" style="text-align: center;">';                    
                    evt += '<div class="easy-block-v1-badge rgba-'+colores[i]+'" style="color:#fff; padding: 5px;">';
                    evt += formatDate(evento.fecha);
                    evt += '</div>';
                    evt += '<em class="overflow-hidden">';
                    evt += '<img class="img-responsive" src="rsc/img/construccion.jpg" alt="Imagen de eventos">';
                    evt += '</em>';
                    evt += '<span>';
                    evt += '<strong>' + evento.nombre + '</strong>';
                    evt += '<i>' + evento.lugar + '</i>';
                    evt += '</span>';
                    evt += '</a>';
                    evt += '</div>';
                    evt += '</div>';    
                    i++;                 
                });
                $('#contenedor-eventos').html(evt);
            }
        }
        ).fail(function (error) {
            console.log(error);
        })
}
loadEventsFirst();

function formatDate(fecha) {
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };    
    fechaSplit = fecha.split(' ');
    fecha = new Date(fechaSplit[0]);    
    nuevaFecha = fecha.toLocaleDateString("es-ES", options);
    console.log(nuevaFecha);
    return nuevaFecha;
}
