// //JQUERY 1.11.3 Por eso uso el promise asi y no con el .then y el .catch

function loadEventsSecond() {
    EventModule.getEvents().promise()
        .done(function (response) {
            console.log(response);
            response.forEach(evento => {
                dia = solveDate(evento.fecha);
                if(dia!=0){
                    if (dia < 10) {
                        $('#td' + dia.substr(1, 2)).addClass('cell-today');
                        $('#data' + dia.substr(1, 2)).html(evento.nombre);
                    } else {
                        $('#td' + dia).addClass('cell-today');
                        $('#data' + dia).html(evento.nombre);
                    }
                }                
                console.log('dia: ' + dia);
            });
        }
        ).fail(function (error) {
            console.log(error);
        })
}
loadEventsSecond();

function solveDate(fechaCompleta) {
    fechaGuion = fechaCompleta.split(' ');
    fecha = fechaGuion[0].split('-');
    dia = fecha[2];
    mes = fecha[1];
    año = fecha[0];
    mesActual = 12;
    
    if(mes!=mesActual){
        return 0;
    }
    return dia;
}

