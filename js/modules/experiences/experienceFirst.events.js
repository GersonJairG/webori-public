//JQUERY 1.11.3 Por eso uso el promise asi y no con el .then y el .catch

function loadExperiencesFirst() {
    ExperienceModule.getExperiences().promise()
        .done(function (response) {
            console.log(response);
            var exp = '';
            if (response[5] != null) {
                console.log('Ponga los primeros 6');                
                var i = 0;
                while (i <= 5) {
                    exp += '<li class="col-md-2 col-sm-3 col-xs-6 md-margin-bottom-30" style="padding-left: 14px;">';
                    exp += '<div class="easy-block-v1">';
                    exp += '<a href="#/experiencias">';
                    exp += '<img src="rsc/img/construccion.jpg" alt="">';
                    exp += '</a>';
                    exp += '</div>';
                    exp += '<div class="block-grid-v2-info rounded-bottom bloques_eventos">';
                    exp += '<h5>';
                    if(response[i].titulo.length > 70){
                        exp += '<b>' + response[i].titulo.substr(0,70) + '...</b>';    
                    }else{
                        exp += '<b>' + response[i].titulo + '</b>';
                    }
                    
                    exp += '</h5>';
                    exp += '</div>';
                    exp += '</li>';
                    i++;
                    console.log('experiencia:' + response[i].id + ', tiene un titulo de :' + response[i].titulo.length + ' caracteres.' )
                }
                $('#contenedor-experiencias').html(exp);            
            } else {
                console.log('Recorra los que haya e imprima');
                response.forEach(experiencia => {
                    exp += '<li class="col-md-2 col-sm-3 col-xs-6 md-margin-bottom-30" style="padding-left: 14px;">';
                    exp += '<div class="easy-block-v1">';
                    exp += '<a href="#/experiencias">';
                    exp += '<img src="rsc/img/construccion.jpg" alt="">';
                    exp += '</a>';
                    exp += '</div>';
                    exp += '<div class="block-grid-v2-info rounded-bottom bloques_eventos">';
                    exp += '<h5>';
                    if(experiencia.titulo.length > 70){
                        exp += '<b>' + experiencia.titulo.substr(0,70) + '...</b>';    
                    }else{
                        exp += '<b>' + experiencia.titulo + '</b>';
                    }                
                    exp += '</h5>';
                    exp += '</div>';
                    exp += '</li>';
                    console.log('experiencia:' + experiencia.id + ', tiene un titulo de :' + experiencia.titulo.length + ' caracteres.' )
                });
                $('#contenedor-experiencias').html(exp); 
            }
        }
        ).fail(function (error) {
            console.log(error);
        })
}
loadExperiencesFirst();

