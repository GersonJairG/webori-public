//JQUERY 1.11.3 Por eso uso el promise asi y no con el .then y el .catch

function loadExperiencesSecond() {
    ExperienceModule.getExperiences().promise()
        .done(function (response) {
            console.log(response);
            var exp = '';    
            response.forEach(experiencia => {
                exp += '<div class="items-row cols-167 row-0">';
                exp += '<div class="item-information column-1">';
                exp += '<div class="img-intro-right">';
                exp += '<img src="rsc/img/construccion.jpg" alt="" class="imgInformacion">';
                exp += '</div>';
                exp += '<div class="float_content">';
                exp += '<h1 class="tituloinformacion">';
                exp += '<a class="anchorinformacion" href="#">';
                exp += experiencia.titulo;
                exp += '</a>';
                exp += '</h1>';
                exp += '<p style="text-align: justify;"></p>';
                var parrafos = experiencia.descripcion.split("\n");
                for (var i =0; i<parrafos.length; i++) {
                    if(i==parrafos.length-1){
                        exp += parrafos[i];                    
                    }else{
                        exp += parrafos[i];
                        exp +='<br><br>';
                    }                
                }                
                exp += '</p>';                
                exp += '</div>';            
                exp += '<span class="row-separator"></span>';
                exp += '</div>';               
                exp += '</div>';                
            });
            $('#contenedor-experiencias-second').html(exp);

        }
        ).fail(function (error) {
            console.log(error);
        })
}
loadExperiencesSecond();

